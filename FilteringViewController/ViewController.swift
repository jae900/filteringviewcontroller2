//
//  ViewController.swift
//  FilteringViewController
//
//  Created by Jae Ki Lee on 22/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let failCountFilterView: UIView = {
       let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let trackFilterView: UIView = {
        let view = UIView()
        view.backgroundColor = .yellow
        return view
    }()
    
    let selfCheckFilterView: UIView = {
        let view = UIView()
        view.backgroundColor = .blue
        return view
    }()
    
    let filteringButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .green
        return button
    }()
    
    
    fileprivate func setupNavigation() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = "필터"
    }
    
    fileprivate func setupViews() {
        let safeLayout = self.view.safeAreaLayoutGuide
        let safeLayoutFrame = self.view.safeAreaLayoutGuide.layoutFrame
        let safeLayoutHeight = safeLayoutFrame.height
        let safeLayoutWidth = safeLayoutFrame.width
        
        self.view.addSubview(failCountFilterView)
        self.view.addSubview(trackFilterView)
        self.view.addSubview(selfCheckFilterView)
        self.view.addSubview(filteringButton)
        
        failCountFilterView.anchor(top: safeLayout.topAnchor, left: safeLayout.leftAnchor, bottom: nil, right: safeLayout.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: self.view.frame.height * 0.137)
        trackFilterView.anchor(top: failCountFilterView.bottomAnchor, left: safeLayout.leftAnchor, bottom: nil, right: safeLayout.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: self.view.frame.height * 0.17)
        selfCheckFilterView.anchor(top: trackFilterView.bottomAnchor, left: safeLayout.leftAnchor, bottom: nil, right: safeLayout.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: self.view.frame.height * 0.355)
        filteringButton.anchor(top: selfCheckFilterView.bottomAnchor, left: safeLayout.leftAnchor, bottom: nil, right: safeLayout.rightAnchor, paddingTop: self.view.frame.height * 0.031, paddingLeft: self.view.frame.width * 0.067, paddingBottom: 0, paddingRight: self.view.frame.width * 0.067, width: self.view.frame.width * 0.087, height: self.view.frame.height * 0.062)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupNavigation()
        setupViews()
        
    }


}

